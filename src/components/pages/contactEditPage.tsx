import { useParams } from "react-router-dom";
import { useContactEdit } from "@hooks/contacts/useContactEdit";

export const ContactEditPage: React.FC = async () => {
  const params = useParams();
  const { contact, update } = await useContactEdit(params.id);
  return <div>{contact.email}</div>;
};
