import { IPerson } from "@lib/models/person";
import { useCallback } from "react";
import { contactsClient } from "@lib/clients/contacts";

export async function useContactEdit(id: string) {
  const contact = await contactsClient.getContactById(id);
  return {
    contact,
    update: useCallback((updated: IPerson) => {
      console.log(updated);
      throw new Error("Not implemented!");
    }, []),
  };
}
